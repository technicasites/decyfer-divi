<?php 

add_action( 'wp_enqueue_scripts', 'enqueue_child_theme_styles');
function enqueue_child_theme_styles() {
	wp_enqueue_style( 'Parents_theme_style', get_template_directory_uri().'/style.css' );
	wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
	if(is_singular('product')){
		wp_enqueue_style( 'flex-style', get_stylesheet_directory_uri() . '/inc/jquery.bxslider.css' );
		wp_enqueue_script( 'flex-script', get_stylesheet_directory_uri() .  '/inc/jquery.bxslider.min.js', array(), '1.0.0', true );
	}

}

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 12);
add_action('woocommerce_single_product_summary', 'custom_product_sku', 7);

function custom_product_sku(){
	global $product;
	echo('<div class="custom_sku">SKU:'.$product->get_sku().'</div>');
}


add_action('woocommerce_single_product_summary', 'custom_thumbnil_img', 13);
function custom_thumbnil_img(){
	global $post, $product, $woocommerce;

	$attachment_ids = $product->get_gallery_attachment_ids();

	if ( $attachment_ids ) {
		$loop 		= 0;
		$columns 	= apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
		echo('<ul class="thum_image">');

		foreach ( $attachment_ids as $attachment_id ) {
			$classes = array( 'zoom' );

			if ( $loop === 0 || $loop % $columns === 0 ) {
				$classes[] = 'first';
			}

			if ( ( $loop + 1 ) % $columns === 0 ) {
				$classes[] = 'last';
			}

			$image_class = implode( ' ', $classes );
			$props       = wc_get_product_attachment_props( $attachment_id, $post );

			if ( ! $props['url'] ) {
				continue;
			}

			$image_attributes = wp_get_attachment_image_src( $attachment_id, 'large' );
			echo apply_filters(
				'woocommerce_single_product_image_thumbnail_html',
				sprintf('<li data-image="'.$image_attributes[0].'">'.wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'medium' ), 0, $props ).'</li>'),
				$attachment_id,
				$post->ID,
				esc_attr( $image_class )
			);
			$loop++;
		}
		echo('</ul>');
	}	
}

add_action('woocommerce_single_product_summary', 'custom_socila_share', 15);
function custom_socila_share( $post_id)
{
	// Get current page URL 
	$share_url = get_permalink($post_id);
	$share_content = get_the_content($post_id);

	// Get current page title
	$share_title = str_replace( ' ', '%20', get_the_title($post_id));

	// Get Post Thumbnail for pinterest
	$share_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'full' );

	// Construct sharing URL without using any script
	$twitterURL = 'https://twitter.com/intent/tweet?text='.$share_title.'&amp;url='.$share_url;
	$facebookURL = 'https://www.facebook.com/sharer/sharer.php?image='.$share_img[0].'&amp;u='.$share_url.'&amp;title='.$share_title.'&amp;description='.$share_content;
	$googleURL = 'https://plus.google.com/share?url='.$share_url;
	$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$share_url.'&amp;media='.$share_img[0].'&amp;description='.$share_title;
	$emailURL ='mailto:?subject='.$share_title.'&amp;body=Link:'.$share_url;

	$content .= '<div class="social_share">';
	$content .='<span class="share">Share</span>';
	$content .= '<a class="facebook" href="'.$facebookURL.'" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>';
	$content .= '<a class="twitter" href="'. $twitterURL .'" target="_blank"><i class="fa fa-twitter"></i></a>';	
	$content .= '<a class="google_plus" href="'.$googleURL.'" target="_blank"><i class="fa fa-google-plus"></i></a>';
	$content .= '<a class="pinterest" href="'.$pinterestURL.'" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a>';
	$content .= '<a class="email" href="'.$emailURL.'" target="_blank"><i class="fa fa-envelope" aria-hidden="true"></i></a>';
	$content .= '</div>';

	echo $content;
}

add_action('wp_head', 'add_featured_slider_script');
function add_featured_slider_script(){
	if(is_singular('product')){
?>
		<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('.bxslider').bxSlider({
			mode: 'vertical',
				pager: false,
				nextText: "",
				prevText: "",
				minSlides: 4,
				maxSlides: 4,
				slideWidth: 120,
				slideMargin: 10
		});

			jQuery(".image_left_slider .bxslider li a").on( "click", function(event) {
				event.preventDefault();
				var image = jQuery(this).attr("data-image");
				jQuery(".images .woocommerce-main-image img").removeAttr("srcset");
				jQuery(".images .woocommerce-main-image img").attr("src", image);
			});

			jQuery(".summary .thum_image li").on( "click", function(event) {
				event.preventDefault();
				var image = jQuery(this).attr("data-image");
				jQuery(".images .woocommerce-main-image img").removeAttr("srcset");
				jQuery(".images .woocommerce-main-image img").attr("src", image);
				jQuery(".summary .thum_image li").css("opacity", 0.5);
				jQuery(this).css("opacity", 1);
			});
		});
		</script>
<?php }
}

add_action('woocommerce_shop_loop_item_title', 'add_product_tage', 15);
function add_product_tage(){
	global $post;
	echo '<div class="product-color">' . get_post_meta($post->ID, 'product_color',true) . '</div>';
}
