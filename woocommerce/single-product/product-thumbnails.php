<?php
/**
 * Single Product Thumbnails
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.6.3
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product, $woocommerce;

$attachment_ids = $product->get_gallery_attachment_ids();

if ( $attachment_ids ) {
	$loop 		= 0;
	$columns 	= apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
?>
   <div class="image_left_slider">
	<ul class="bxslider">
<?php
	$counter = 1;
	foreach ( $attachment_ids as $attachment_id ) {

		$classes = array( 'zoom' );

		if ( $loop === 0 || $loop % $columns === 0 ) {
			$classes[] = 'first';
		}

		if ( ( $loop + 1 ) % $columns === 0 ) {
			$classes[] = 'last';
		}

		$image_class = implode( ' ', $classes );
		$props       = wc_get_product_attachment_props( $attachment_id, $post );

		if ( ! $props['url'] ) {
			continue;
		}

		$image_attributes = wp_get_attachment_image_src( $attachment_id, 'large' );

		if($counter == 1) {

			echo apply_filters(
				'woocommerce_single_product_image_thumbnail_html',
				sprintf(
					'<li><a href="%s" class="%s" title="%s" data-image="%s">%s</a></li>',
					esc_url( $props['url'] ),
					esc_attr( $image_class ),
					esc_attr( $props['caption'] ),
					$image_attributes[0],
					wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'medium' ), 0, $props )
				),
				$attachment_id,
				$post->ID,
				esc_attr( $image_class )
			);

		} else {
			echo '<li><div class="blank-product">&nbsp;</div></li>';
		}
		$counter++;
		$loop++;
	}
?>
    </ul>
    </div>
<?php
}
